<?php
class values{
    protected $title="Default Title";
    protected $body="Default Body";
    function __construct($title,$body){
        if($title != ""){
            $this->title = $title;
        }
        if($body != ""){
            $this->body = $body;
        }
    }
} 

class colorMessage extends values {
    protected $color = 'black';
    public function __set($property,$value){
        if($property == 'color'){
            $colors = array('green','blue','gray');
            if(in_array($value, $colors)){
                $this->color = $value;
            }
            else{
                $this->body = "This color is invalid";
            }
        }
    }
}

class fontSize extends colorMessage{
    protected $size;
    public function __set($property,$value){
        parent::__set($property,$value);
            if($property == 'size'){
                if(in_array($value, range(10,24))){
                    $this->size = $value.'px';
                } 
                elseif($this->body == "This color is invalid"){
                        $this->body = "color and font size are invalid";
                } 
                else{
                    $this->body = 'This font size is invalid';
                } 
            }
    }         


    public function view(){
        return "<!DOCTYPE html>
        <html>
        <head>
            <title> $this->title </title>
        </head>
        <body><p style = 'color:$this->color;font-size:$this->size'>$this->body</p> </body>
        </html>";
    }
}

?>



